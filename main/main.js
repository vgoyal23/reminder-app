const { app, BrowserWindow, Notification, ipcMain } = require('electron');
const path = require('path');
const { MenuBuilder } = require('./menu');

function createWindow () {
	app.allowRendererProcessReuse = true;
  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    resizable: false,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: true, 
      enableRemoteModule: true,
      preload: path.join(__dirname, 'preload.js')
    }
  });

	mainWindow.loadFile('renderer/index.html');

  mainWindow.on('ready-to-show', () => {
    if (!mainWindow) {
      throw new Error('"mainWindow" is not defined');
    }

    mainWindow.show();
  });

  const menuBuilder = new MenuBuilder(mainWindow);
  menuBuilder.buildMenu();
};

ipcMain.on('set-reminder', (event, args) => {
  const { title, time } = args;
  function showNotification() {
    new Notification({
      title: 'Reminder',
      body: title,
      icon: path.join(__dirname, '../assets/reminder.png'),
    }).show();
  }

  setTimeout(() => {
    showNotification();
  }, time);
});

/**
 * Add event listeners...
 */

app.on('window-all-closed', () => {
  app.quit();
});

app.whenReady().then(createWindow);
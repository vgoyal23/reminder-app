const { Menu } = require('electron');

class MenuBuilder {
  buildMenu() {
    const template = [];
    const menu = Menu.buildFromTemplate(template);
    Menu.setApplicationMenu(menu);

    return menu;
  }
}

exports.MenuBuilder = MenuBuilder;

const titleElement = document.getElementById("title");
const timeElement = document.getElementById("time");
const button = document.getElementById("submit");

const getMilliSecs = (time) => {
  const currentDate = new Date();
  const newDate = new Date();

  const timeDetails = time.split(":");
  newDate.setHours(timeDetails[0]);
  newDate.setMinutes(timeDetails[1]);
  newDate.setSeconds(00);

  return Math.abs(newDate - currentDate);
};

const onSetReminder = () => {
  const timeValue = getMilliSecs(timeElement.value);

  window.electron.ipcRenderer.sendMessage('set-reminder', {
    title: titleElement.value,
    time: timeValue,
  });
};

button.onclick = onSetReminder;